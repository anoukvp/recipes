import mongoose from 'mongoose'

const recipeSchema = mongoose.Schema({
    name :String,
    difficulty : String,
    withOven : Boolean,
    price : Number
})

const recipeModel = mongoose.model("Recipe",recipeSchema)

export { recipeModel,recipeSchema}