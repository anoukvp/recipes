import express from "express"
import mongoose from 'mongoose'
import dotenv from 'dotenv'
import cors from 'cors'
import { recipeModel as Recipe} from './recipes.js'

dotenv.config()

// CORS
const app = express()
app.use(cors('*'))
app.set('views',"./views")
app.set('view engine','pug')

    
app.use("/static",express.static("./images"))

// Routes

app.get('/home', async (req, res, next) => {
    res.render("homePage")
    // next()
})

app.get('/recipes', async (req, res, next) => {
    const recipes = await Recipe.find({})
  //  console.log("recipe",recipes);
    res.render("indexRecipe",{recipes:recipes})
    // next()
})

app.get('/recipes/:id/edit', async (req, res, next) => {
    const recipe = await Recipe.findOne({_id:req.params.id})
    res.render("editRecipe",{recipe})
    // next()
})



app.listen(4000, () => {
    console.log("serveur démarré sur le port 4000...");
    mongoose.connect(process.env.DB).then(()=>{
        console.log("connecté à mongoDB!");
    })
})